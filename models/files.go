package models

import (
	"encoding/json"
	"log"
)

/*
branch	string	yes	Name of the new branch to create. The commit is added to this branch.
commit_message	string	yes	The commit message.
content	string	yes	The file’s content.
file_path	string	yes	URL-encoded full path to new file. For example: lib%2Fclass%2Erb.
id	integer or string	yes	The ID or URL-encoded path of the project owned by the authenticated user.
author_email	string	no	The commit author’s email address.
author_name	string	no	The commit author’s name.
encoding	string	no	Change encoding to base64. Default is text.
execute_filemode	boolean	no	Enables or disables the execute flag on the file. Can be true or false.
start_branch	string	no	Name of the base branch to create the new branch from.

POST /projects/:id/repository/files/:file_path

*/

type File struct {
	Branch        string `json:"branch,omitempty"`
	CommitMessage string `json:"commit_message,omitempty"`
	Content       string `json:"content,omitempty"`
	FilePath      string `json:"file_path,omitempty"`
	ProjectId     int    `json:"id,omitempty"`
}

func (file File) ToJsonString() string {
	bjson, err := json.Marshal(file)
	if err != nil {
		log.Println("😡(File.ToJsonString)", err.Error())
		return `{}`
	}
	return string(bjson)
}
