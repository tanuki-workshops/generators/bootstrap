package models

import (
	"encoding/json"
	"log"
)

// epic_id epic_iid

type Issue struct {
	Title       string   `json:"title,omitempty"`
	Description string   `json:"description,omitempty"`
	CreatedAt   string   `json:"created_at,omitempty"`
	DueDate     string   `json:"due_date,omitempty"`
	Id          int      `json:"id,omitempty"`
	IId         int      `json:"iid,omitempty"`
	Labels      []string `json:"labels,omitempty"`
	Weight      int      `json:"weight,omitempty"`

	HealthStatus string `json:"health_status,omitempty"`

	EpicId int `json:"epic_id,omitempty"`

	MilestoneId int `json:"milestone_id,omitempty"`

	Project Project
}

func (issue Issue) ToJsonString() string {
	bjson, err := json.Marshal(issue)
	if err != nil {
		log.Println("😡(Issue.ToJsonString)", err.Error())
		return `{}`
	}
	return string(bjson)
}
