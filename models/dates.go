package models

import "time"

type SimpleDate struct {
	Year  int
	Month time.Month
	Day   int
}

func (simpleDate SimpleDate) ToDateString() string {
	return time.Date(simpleDate.Year, simpleDate.Month, simpleDate.Day, 10, 0, 0, 0, time.UTC).Format(time.DateTime)
}
