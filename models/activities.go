package models

type SubDevelopmentActivities struct {
	Tools Epic
	Helpers Epic
	BusinessRules Epic
}


type MainActivities struct {
	ProjectManagement Epic
	Analysis Epic
	Design Epic
	Development Epic
	TestRelease Epic
}
