package models

type Phases struct {
	Inception Milestone
	Elaboration Milestone
	Construction Milestone
	Transition Milestone
}
