package models

import (
	"encoding/json"
	"log"
)

type Group struct {
	Id         int    `json:"id,omitempty"`
	Name       string `json:"name,omitempty"`
	Path       string `json:"path,omitempty"`
	ParentId   int    `json:"parent_id,omitempty"`
	Visibility string `json:"visibility,omitempty"`
}

func (group Group) ToJsonString() string {
	bjson, err := json.Marshal(group)
	if err != nil {
		log.Println("😡(Group.ToJsonString)", err.Error())
		return `{}`
	}
	return string(bjson)
}
