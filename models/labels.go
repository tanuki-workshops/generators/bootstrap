package models

import (
	"encoding/json"
	"log"
)

type Label struct {
	Name        string `json:"name"`
	Color       string `json:"color"`
	Description string `json:"description"`
}

func (label Label) ToJsonString() string {
	bjson, err := json.Marshal(label)
	if err != nil {
		log.Println("😡(Label.ToJsonString)", err.Error())
		return `{}`
	}
	return string(bjson)
}
