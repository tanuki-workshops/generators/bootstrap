package models

import (
	"encoding/json"
	"log"
)

type Milestone struct {
	Id int `json:"id,omitempty"`

	Title       string `json:"title,omitempty"`
	Description string `json:"description,omitempty"`
	DueDate     string `json:"due_date,omitempty"`
	StartDate   string `json:"start_date,omitempty"`

	Project Project
	Group Group
}

func (milestone Milestone) ToJsonString() string {
	bjson, err := json.Marshal(milestone)
	if err != nil {
		log.Println("😡(Milestone.ToJsonString)", err.Error())
		return `{}`
	}
	return string(bjson)
}
