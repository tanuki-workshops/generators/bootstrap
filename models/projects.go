package models

import (
	"encoding/json"
	"log"
)

type Project struct {
	Id                   int    `json:"id,omitempty"`
	Name                 string `json:"name,omitempty"`
	NameSpaceId          int    `json:"namespace_id,omitempty"`
	InitializeWithReadMe bool   `json:"initialize_with_readme,omitempty"`
	Visibility           string `json:"visibility,omitempty"`
}

func (project Project) ToJsonString() string {
	bjson, err := json.Marshal(project)
	if err != nil {
		log.Println("😡(Project.ToJsonString)", err.Error())
		return `{}`
	}
	return string(bjson)
}
