package models

import (
	"encoding/json"
	"log"
)

type Epic struct {
	Title       string   `json:"title,omitempty"`
	Description string   `json:"description,omitempty"`
	Color       string   `json:"color,omitempty"`
	Labels      []string `json:"labels,omitempty"`

	CreatedAt string `json:"created_at,omitempty"`

	StartDateIsFixed bool   `json:"start_date_is_fixed,omitempty"`
	StartDateFixed   string `json:"start_date_fixed,omitempty"`

	// start_date_is_fixed boolean
	// start_date_fixed string
	// due_date_is_fixed boolean
	// due_date_fixed string

	ParentId int `json:"parent_id,omitempty"`
	Id       int `json:"id,omitempty"`
	IId      int `json:"iid,omitempty"`

	Group Group
}

func (epic Epic) ToJsonString() string {
	bjson, err := json.Marshal(epic)
	if err != nil {
		log.Println("😡(Epic.ToJsonString)", err.Error())
		return `{}`
	}
	return string(bjson)
}
