package main

import (
	"fmt"
	"glhelper/models"
	"testing"
)

func TestFile(t *testing.T) {

	file, err := gl.PublishFile(models.File{
		Branch:        "main",
		CommitMessage: "add file",
		Content:       `tada!!!`,
		FilePath:      ".hello%2Fgreetings.txt",
		ProjectId:     51531599,
	})

	if err != nil {
		t.Errorf("File Error: %q", err)
	}
	fmt.Println(file)

}
