FROM gitpod/workspace-full
USER gitpod

#FROM ubuntu:latest

LABEL maintainer="@k33g_org"

ARG WORKSPACE_ARCH=amd64
ARG GO_VERSION=1.21.3

ARG DEBIAN_FRONTEND=noninteractive

# Update the system and install necessary tools
RUN <<EOF
sudo apt-get update 
sudo apt-get install -y curl wget git build-essential xz-utils bat exa software-properties-common redis
#add-apt-repository ppa:deadsnakes/ppa -y
#apt-get update 
#apt-get install -y python3.8
sudo ln -s /usr/bin/batcat /usr/bin/bat
EOF

# ------------------------------------
# Install Go
# ------------------------------------
RUN <<EOF
GO_ARCH="${WORKSPACE_ARCH}"

wget https://golang.org/dl/go${GO_VERSION}.linux-${GO_ARCH}.tar.gz
tar -xvf go${GO_VERSION}.linux-${GO_ARCH}.tar.gz
sudo mv go /usr/local
EOF

# ------------------------------------
# Set Environment Variables for Go
# ------------------------------------
ENV GOROOT=/usr/local/go
ENV GOPATH=$HOME/go
ENV PATH=$GOPATH/bin:$GOROOT/bin:$PATH

RUN <<EOF
go version
go install -v golang.org/x/tools/gopls@latest
go install -v github.com/ramya-rao-a/go-outline@latest
go install -v github.com/stamblerre/gocode@v1.0.0
EOF

# Command to run when starting the container
#CMD ["/bin/bash"]
