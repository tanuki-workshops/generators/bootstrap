package tasks

// https://en.wikipedia.org/wiki/Unified_Process
import (
	"fmt"
	"glhelper/client"
	"glhelper/models"
	"time"
)

func CreateProjectMilestones(gl client.GitLabClient, project models.Project, startDate time.Time) (models.Phases, error) {

	inceptionStartDate := startDate
	inceptionEndDate := startDate.Add(10 * 24 * time.Hour)

	inception, err := gl.PublishProjectMilestone(models.Milestone{
		Title:       "🤔 Inception",
		Description: "Inception is the smallest phase in the project, and ideally, it should be quite short.",
		StartDate:   inceptionStartDate.Local().String(),
		DueDate:     inceptionEndDate.Local().String(),
		Project:     project,
	})

	if err != nil {
		fmt.Println("😡 error when publishing the inception milestone", err.Error())
	}

	fmt.Println("🔴", inception)


	elaborationStartDate := inceptionEndDate
	elaborationEndDate := elaborationStartDate.Add(10 * 24 * time.Hour)

	elaboration, err := gl.PublishProjectMilestone(models.Milestone{
		Title:       "🧩 Elaboration",
		Description: "During the elaboration phase, the project team is expected to capture a healthy majority of the system requirements.",
		StartDate:   elaborationStartDate.Local().String(),
		DueDate:     elaborationEndDate.Local().String(),
		Project:     project,
	})

	if err != nil {
		fmt.Println("😡 error when publishing the elaboration milestone", err.Error())
	}

	fmt.Println("🟠", elaboration)

	constructionStartDate := elaborationEndDate
	constructionEndDate := constructionStartDate.Add(20 * 24 * time.Hour)

	construction, err := gl.PublishProjectMilestone(models.Milestone{
		Title:       "🚧 Construction",
		Description: "Construction is the largest phase of the project. In this phase, the remainder of the system is built on the foundation laid in elaboration.",
		StartDate:   constructionStartDate.Local().String(),
		DueDate:     constructionEndDate.Local().String(),
		Project:     project,
	})

	if err != nil {
		fmt.Println("😡 error when publishing the construction milestone", err.Error())
	}

	fmt.Println("🟣", construction)

	transitionStartDate := constructionEndDate
	transitionEndDate := transitionStartDate.Add(5 * 24 * time.Hour)

	transition, err := gl.PublishProjectMilestone(models.Milestone{
		Title:       "🏓 Transition",
		Description: "The final project phase is transition. In this phase the system is deployed to the target users.",
		StartDate:   transitionStartDate.Local().String(),
		DueDate:     transitionEndDate.Local().String(),
		Project:     project,
	})

	if err != nil {
		fmt.Println("😡 error when publishing the transition milestone", err.Error())
	}

	fmt.Println("🟢", transition)


	return models.Phases{
		Inception:    inception,
		Elaboration:  elaboration,
		Construction: construction,
		Transition:   transition,
	}, err
}
