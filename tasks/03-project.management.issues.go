package tasks

import (
	"fmt"
	"glhelper/client"
	"glhelper/models"
	"time"
)

func CreateProjectManagementIssues(gl client.GitLabClient, projectManagementEpic models.Epic, project models.Project, group models.Group, phases models.Phases, startDate time.Time) {


	projectManagementPlanIssue, err := gl.PublishIssue(models.Issue{
		Title:       "Project management plan",
		Description: "Project management plan",
		CreatedAt:   startDate.Add(1 * 24 * time.Hour).Local().String(),
		Project:     project,
		Labels:      []string{"task", "project management", "priority::1"},
		EpicId:      projectManagementEpic.Id,
		MilestoneId: phases.Inception.Id,
		Weight:      5,
	})
	if err != nil {
		fmt.Println("😡", err.Error())
	}

	scopeStatementIssue, err := gl.PublishIssue(models.Issue{
		Title:       "Scope statement",
		Description: "Scope statement",
		CreatedAt:   startDate.Add(2 * 24 * time.Hour).Local().String(),
		Project:     project,
		Labels:      []string{"task", "project management", "priority::1"},
		EpicId:      projectManagementEpic.Id,
		MilestoneId: phases.Inception.Id,
		Weight:      5,
	})
	if err != nil {
		fmt.Println("😡", err.Error())
	}
	_, _ = gl.LinkIssues(scopeStatementIssue, projectManagementPlanIssue)

	schedulingIssue, err := gl.PublishIssue(models.Issue{
		Title:       "Scheduling",
		Description: "Scheduling",
		CreatedAt:   startDate.Add(2 * 24 * time.Hour).Local().String(),
		Project:     project,
		Labels:      []string{"task", "project management", "priority::1"},
		EpicId:      projectManagementEpic.Id,
		MilestoneId: phases.Inception.Id,
		Weight:      5,
		HealthStatus: "on_track", // on_track at_risk needs_attention
	})
	if err != nil {
		fmt.Println("😡", err.Error())
	}
	_, _ = gl.LinkIssues(schedulingIssue, projectManagementPlanIssue)

	riskPlanningIssue, err := gl.PublishIssue(models.Issue{
		Title:       "Risk planning",
		Description: "Risk planning",
		CreatedAt:   startDate.Add(1 * 24 * time.Hour).Local().String(),
		Project:     project,
		Labels:      []string{"task", "project management", "priority::1"},
		EpicId:      projectManagementEpic.Id,
		MilestoneId: phases.Inception.Id,
		Weight:      5,
		HealthStatus: "needs_attention", // on_track at_risk needs_attention

	})
	if err != nil {
		fmt.Println("😡", err.Error())
	}
	_, _ = gl.LinkIssues(riskPlanningIssue, projectManagementPlanIssue)

	planChangesIssue, err := gl.PublishIssue(models.Issue{
		Title:       "Plan changes",
		Description: "Plan changes",
		CreatedAt:   startDate.Add(3 * 24 * time.Hour).Local().String(),
		Project:     project,
		Labels:      []string{"task", "project management"},
		EpicId:      projectManagementEpic.Id,
		MilestoneId: phases.Elaboration.Id,
		Weight:      25,
		HealthStatus: "on_track", // on_track at_risk needs_attention

	})
	if err != nil {
		fmt.Println("😡", err.Error())
	}
	_, _ = gl.LinkIssues(planChangesIssue, projectManagementPlanIssue)

	_, err = gl.PublishIssue(models.Issue{
		Title:       "Inception Project management",
		Description: "Inception Project management",
		CreatedAt:   startDate.Add(5 * 24 * time.Hour).Local().String(),
		Project:     project,
		Labels:      []string{"task", "project management"},
		EpicId:      projectManagementEpic.Id,
		MilestoneId: phases.Inception.Id,
		Weight:      10,
		HealthStatus: "needs_attention", // on_track at_risk needs_attention

	})
	if err != nil {
		fmt.Println("😡", err.Error())
	}

	_, err = gl.PublishIssue(models.Issue{
		Title:       "Elaboration Project management",
		Description: "Elaboration Project management",
		CreatedAt:   startDate.Add(5 * 24 * time.Hour).Local().String(),
		Project:     project,
		Labels:      []string{"task", "project management"},
		EpicId:      projectManagementEpic.Id,
		MilestoneId: phases.Elaboration.Id,
		Weight:      10,
		HealthStatus: "on_track", // on_track at_risk needs_attention

	})
	if err != nil {
		fmt.Println("😡", err.Error())
	}

	_, err = gl.PublishIssue(models.Issue{
		Title:       "Construction Project management",
		Description: "Construction Project management",
		CreatedAt:   startDate.Add(5 * 24 * time.Hour).Local().String(),
		Project:     project,
		Labels:      []string{"task", "project management"},
		EpicId:      projectManagementEpic.Id,
		MilestoneId: phases.Construction.Id,
		Weight:      10,
		HealthStatus: "on_track", // on_track at_risk needs_attention

	})
	if err != nil {
		fmt.Println("😡", err.Error())
	}

	_, err = gl.PublishIssue(models.Issue{
		Title:       "Transition Project management",
		Description: "Transition Project management",
		CreatedAt:   startDate.Add(5 * 24 * time.Hour).Local().String(),
		Project:     project,
		Labels:      []string{"task", "project management"},
		EpicId:      projectManagementEpic.Id,
		MilestoneId: phases.Transition.Id,
		Weight:      10,
		HealthStatus: "on_track", // on_track at_risk needs_attention

	})
	if err != nil {
		fmt.Println("😡", err.Error())
	}

}
