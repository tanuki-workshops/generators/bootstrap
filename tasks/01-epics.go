package tasks

import (
	"fmt"
	"glhelper/client"
	"glhelper/models"
)

// Epics should start at the same time ?
// , year int, month time.Month, day int
func CreateEpics(gl client.GitLabClient, group models.Group) (models.MainActivities, models.SubDevelopmentActivities, error) {

	mainEpic, err := gl.PublishEpic(models.Epic{
		Title:       "🚀 Nostromo project",
		Description: "Alien reboot 👽",
		Color:       "#6699cc",
		Group:       group,
	})

	if err != nil {
		fmt.Println("😡 error when publishing the epic", err.Error())
	}
	fmt.Println("🚀", mainEpic)

	projectManagementEpic, err := gl.PublishEpic(models.Epic{
		Title:            "👷 01-Project Management",
		Description:      "Project management. The first stage of our work breakdown structure for software project example includes planning, defining scope, scheduling, risk management, and work with possible plan changes.",
		Group:            group,
		Color:            "Green",
		Labels:           []string{"project management", "governance & requirements"},
		ParentId:         mainEpic.Id,
		StartDateIsFixed: false,
		//StartDateFixed:   models.SimpleDate{Year: year, Month: month, Day: day}.ToDateString(),
	})

	if err != nil {
		fmt.Println("😡 error when publishing the epic", err.Error())
	}
	fmt.Println("👷", projectManagementEpic)

	analysisEpic, err := gl.PublishEpic(models.Epic{
		Title:            "📝 02-Analysis",
		Description:      "Analysis. At this stage, project teams conduct required interviews, work on requirements specifications, and prepare use cases.",
		Group:            group,
		Labels:           []string{"analysis", "governance & requirements"},
		Color:            "Blue",
		ParentId:         mainEpic.Id,
		StartDateIsFixed: false,
		//StartDateFixed:   models.SimpleDate{Year: year, Month: month, Day: day+5}.ToDateString(),
	})

	if err != nil {
		fmt.Println("😡 error when publishing the epic", err.Error())
	}

	fmt.Println("📝", analysisEpic)

	designEpic, err := gl.PublishEpic(models.Epic{
		Title:            "🖼️ 03-Design",
		Description:      "Design is one of the most essential parts of our software development work breakdown structure example. Here, you should care about the prototype design, architecture design, and site performance improvements.",
		Group:            group,
		Labels:           []string{"design", "technical & development"},
		Color:            "Orange",
		ParentId:         mainEpic.Id,
		StartDateIsFixed: false,
		//StartDateFixed:   models.SimpleDate{Year: year, Month: month, Day: day +10}.ToDateString(),
	})

	if err != nil {
		fmt.Println("😡 error when publishing the epic", err.Error())
	}

	fmt.Println("🖼️", designEpic)

	developmentEpic, err := gl.PublishEpic(models.Epic{
		Title:            "👨‍💻 04-Development",
		Description:      "Development. This is typically one of the most active phases of software development, so you will need to thoroughly work on developing the new e-commerce site and care about all the details, meaning graphics and interface, content creation, database implementation, catalog engine, transaction processing, iOS and Android integration, security, and other important issues.",
		Group:            group,
		Labels:           []string{"development", "technical & development"},
		Color:            "Purple",
		ParentId:         mainEpic.Id,
		StartDateIsFixed: false,
		//StartDateFixed:   models.SimpleDate{Year: year, Month: month, Day: day +20}.ToDateString(),
	})

	if err != nil {
		fmt.Println("😡 error when publishing the epic", err.Error())
	}

	fmt.Println("🚧", developmentEpic)

	// sub developing epic
	tools, err := gl.PublishEpic(models.Epic{
		Title:            "🛠️ 04-01-Tools",
		Description:      "Tools",
		Group:            group,
		Labels:           []string{"development", "technical & development"},
		Color:            "Purple",
		ParentId:         developmentEpic.Id,
		StartDateIsFixed: false,
	})
	if err != nil {
		fmt.Println("😡 error when publishing the sub epic", err.Error())
	}

	helpers, err := gl.PublishEpic(models.Epic{
		Title:            "🧰 04-02-Helpers",
		Description:      "Helpers",
		Group:            group,
		Labels:           []string{"development", "technical & development"},
		Color:            "Purple",
		ParentId:         developmentEpic.Id,
		StartDateIsFixed: false,
	})
	if err != nil {
		fmt.Println("😡 error when publishing the sub epic", err.Error())
	}

	rules, err := gl.PublishEpic(models.Epic{
		Title:            "📊 04-03-Business Rules",
		Description:      "Business Rules",
		Group:            group,
		Labels:           []string{"development", "technical & development"},
		Color:            "Purple",
		ParentId:         developmentEpic.Id,
		StartDateIsFixed: false,
	})
	if err != nil {
		fmt.Println("😡 error when publishing the sub epic", err.Error())
	}

	subDevelopmentActivities := models.SubDevelopmentActivities{
		Tools: tools,
		Helpers: helpers,
		BusinessRules: rules,
	}

	// end of sub developing epic
	
	testReleaseEpic, err := gl.PublishEpic(models.Epic{
		Title:            "✅ 05-Testing and Releasing",
		Description:      "Testing and Releasing are what end the process. This is when test configuration, reviewing design, releasing the site, closeout meetings, and preparing closeout documents happen. ",
		Group:            group,
		Labels:           []string{"test & release", "software recipe & release"},
		Color:            "Red",
		ParentId:         mainEpic.Id,
		StartDateIsFixed: false,
		//StartDateFixed:   models.SimpleDate{Year: year, Month: month, Day: day + 25}.ToDateString(),
	})

	if err != nil {
		fmt.Println("😡 error when publishing the epic", err.Error())
	}

	fmt.Println("✅", testReleaseEpic)

	mainActivities := models.MainActivities{
		ProjectManagement: projectManagementEpic,
		Analysis: analysisEpic,
		Design: designEpic,
		Development: developmentEpic,
		TestRelease: testReleaseEpic,
	}

	return mainActivities, subDevelopmentActivities, err

}
