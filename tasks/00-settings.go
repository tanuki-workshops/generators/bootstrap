package tasks

import (
	"fmt"
	"glhelper/client"
	"glhelper/models"
)

var readme = `
# Demo
🚧 WIP: This is a work in progress
`

var settings = `{
    "workbench.iconTheme": "material-icon-theme",
    "workbench.colorTheme": "Cobalt2",
    "terminal.integrated.fontSize": 16,
    "editor.fontSize": 16,
    "files.autoSave": "afterDelay",
    "files.autoSaveDelay": 1000,
    "editor.insertSpaces": true,
    "editor.tabSize": 4,
    "editor.detectIndentation": true
}`

var insights = `# insights
# insights
PrioritiesCharts:
  title: "Priorities"
  charts:
    - title: "Priorities"
      description: "Open issues with priority"
      type: bar
      query:
        data_source: issuables
        params:
          issuable_type: issue
          issuable_state: opened
          #filter_labels:
          #  - bug
          collection_labels:
            - "priority::1"
            - "priority::2"
            - "priority::3"
            - "priority::4"
          #group_by: month
    - title: "Monthly Priorities"
      description: "Open issues with priority"
      type: stacked-bar
      query:
        data_source: issuables
        params:
          issuable_type: issue
          issuable_state: opened
          #filter_labels:
          #  - bug
          collection_labels:
            - "priority::1"
            - "priority::2"
            - "priority::3"
            - "priority::4"
          group_by: month
    - title: "Weekly Priorities"
      description: "Open Priorities created per week"
      type: stacked-bar
      query:
        data_source: issuables
        params:
          issuable_type: issue
          issuable_state: opened
          #filter_labels:
          #  - bug
          collection_labels:
            - "priority::1"
            - "priority::2"
            - "priority::3"
            - "priority::4"
          group_by: week

TasksCharts:
  title: "Tasks"
  charts:

    - title: "Tasks"
      description: "Tasks per type"
      type: bar
      query:
        data_source: issuables
        params:
          issuable_type: issue
          issuable_state: opened
          filter_labels:
            - task
          collection_labels:
            - "project management"
            - "analysis"
            - "design"
            - "development"
            - "test & release"


    - title: "Weekly Tasks"
      description: "Tasks per type"
      type: stacked-bar
      query:
        data_source: issuables
        params:
          issuable_type: issue
          issuable_state: opened
          filter_labels:
            - task
          collection_labels:
            - "project management"
            - "analysis"
            - "design"
            - "development"
            - "test & release"
          group_by: week
`

func CreateLabels(gl client.GitLabClient, group models.Group, project models.Project) {

	fmt.Println("🚩 label creation", group)

	// used for the epics
	gl.PublishGroupLabel(models.Label{Name: "project management", Color: "green", Description: ""}, group)
	gl.PublishGroupLabel(models.Label{Name: "analysis", Color: "pink", Description: ""}, group)
	gl.PublishGroupLabel(models.Label{Name: "design", Color: "purple", Description: ""}, group)
	gl.PublishGroupLabel(models.Label{Name: "development", Color: "black", Description: ""}, group)
	gl.PublishGroupLabel(models.Label{Name: "testing & production", Color: "grey", Description: ""}, group)

	// used to group some epics (and create an epic board)
	gl.PublishGroupLabel(models.Label{Name: "governance & requirements", Color: "#36454f", Description: ""}, group)
	gl.PublishGroupLabel(models.Label{Name: "technical & development", Color: "#330066", Description: ""}, group)
	gl.PublishGroupLabel(models.Label{Name: "software recipe & production", Color: "#009966", Description: ""}, group)


	// used for the issues
	gl.PublishGroupLabel(models.Label{Name: "task", Color: "purple", Description: "this is a task"}, group)
	gl.PublishGroupLabel(models.Label{Name: "bug", Color: "red", Description: "this is a bug"}, group)
	gl.PublishGroupLabel(models.Label{Name: "priority::1", Color: "red", Description: "important"}, group)
	gl.PublishGroupLabel(models.Label{Name: "priority::2", Color: "orange", Description: ""}, group)
	gl.PublishGroupLabel(models.Label{Name: "priority::3", Color: "yellow", Description: ""}, group)
	gl.PublishGroupLabel(models.Label{Name: "priority::4", Color: "green", Description: ""}, group)


	gl.PublishFile(models.File{
		Branch:        "main",
		CommitMessage: "📝 add README file",
		Content:       readme,
		FilePath:      "README.md",
		ProjectId:     project.Id,
	})

	gl.PublishFile(models.File{
		Branch:        "main",
		CommitMessage: "📝 add insights file",
		Content:       insights,
		FilePath:      ".gitlab%2Finsights.yml",
		ProjectId:     project.Id,
	})

	gl.PublishFile(models.File{
		Branch:        "main",
		CommitMessage: "📝 add settings file",
		Content:       settings,
		FilePath:      ".vscode%2Fsettings.json",
		ProjectId:     project.Id,
	})



}
