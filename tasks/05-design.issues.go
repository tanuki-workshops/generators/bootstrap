package tasks

import (
	"fmt"
	"glhelper/client"
	"glhelper/models"
	"time"
)

func CreateDesignIssues(gl client.GitLabClient, designEpic models.Epic, project models.Project, group models.Group, phases models.Phases, startDate time.Time) {

	prototypeIssue, err := gl.PublishIssue(models.Issue{
		Title:       "Prototype design",
		Description: "Prototype design",
		CreatedAt:   startDate.Add(6 * 24 * time.Hour).Local().String(),
		Project:     project,
		Labels:      []string{"task", "design", "priority::3"},
		EpicId:      designEpic.Id,
		MilestoneId: phases.Elaboration.Id,
		Weight:      25,
	})
	if err != nil {
		fmt.Println("😡", err.Error())
	}

	architectureIssue, err := gl.PublishIssue(models.Issue{
		Title:       "Architecture design",
		Description: "Architecture design",
		CreatedAt:   startDate.Add(6 * 24 * time.Hour).Local().String(),
		Project:     project,
		Labels:      []string{"task", "design", "priority::3"},
		EpicId:      designEpic.Id,
		MilestoneId: phases.Construction.Id,
		Weight:      25,
	})
	if err != nil {
		fmt.Println("😡", err.Error())
	}
	_, _ = gl.LinkIssues(architectureIssue, prototypeIssue)

	performanceIssue, err := gl.PublishIssue(models.Issue{
		Title:       "Improving site performance",
		Description: "Improving site performance",
		CreatedAt:   startDate.Add(10 * 24 * time.Hour).Local().String(),
		Project:     project,
		Labels:      []string{"task", "design", "priority::4"},
		EpicId:      designEpic.Id,
		MilestoneId: phases.Transition.Id,
		Weight:      35,
	})
	if err != nil {
		fmt.Println("😡", err.Error())
	}
	_, _ = gl.LinkIssues(performanceIssue, prototypeIssue)

}
