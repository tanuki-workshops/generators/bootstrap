package tasks

import (
	"fmt"
	"glhelper/client"
	"glhelper/models"
	"time"
)

func CreateAnalysisIssues(gl client.GitLabClient, analysisEpic models.Epic, project models.Project, group models.Group, phases models.Phases, startDate time.Time) {

	interviewsIssue, err := gl.PublishIssue(models.Issue{
		Title:       "Interviews",
		Description: "Interviews",
		CreatedAt:   startDate.Add(2 * 24 * time.Hour).Local().String(),
		Project:     project,
		Labels:      []string{"task", "analysis", "priority::2"},
		EpicId:      analysisEpic.Id,
		MilestoneId: phases.Inception.Id,
		Weight:      5,
	})
	if err != nil {
		fmt.Println("😡", err.Error())
	}

	requirementsIssue, err := gl.PublishIssue(models.Issue{
		Title:       "Requirements specifications",
		Description: "Requirements specifications",
		CreatedAt:   startDate.Add(2 * 24 * time.Hour).Local().String(),
		Project:     project,
		Labels:      []string{"task", "analysis", "priority::2"},
		EpicId:      analysisEpic.Id,
		MilestoneId: phases.Elaboration.Id,
		Weight:      35,
	})
	if err != nil {
		fmt.Println("😡", err.Error())
	}
	_, _ = gl.LinkIssues(requirementsIssue, interviewsIssue)

	useCasesIssue, err := gl.PublishIssue(models.Issue{
		Title:       "Use cases",
		Description: "Use cases",
		CreatedAt:   startDate.Add(4 * 24 * time.Hour).Local().String(),
		Project:     project,
		Labels:      []string{"task", "analysis", "priority::3"},
		MilestoneId: phases.Elaboration.Id,
		EpicId:      analysisEpic.Id,
		Weight:      15,
	})
	if err != nil {
		fmt.Println("😡", err.Error())
	}
	_, _ = gl.LinkIssues(useCasesIssue, interviewsIssue)

	reportingIssue, err := gl.PublishIssue(models.Issue{
		Title:       "Reporting needs",
		Description: "Reporting needs",
		CreatedAt:   startDate.Add(6 * 24 * time.Hour).Local().String(),
		Project:     project,
		Labels:      []string{"task", "analysis", "priority::3"},
		MilestoneId: phases.Elaboration.Id,
		EpicId:      analysisEpic.Id,
		Weight:      15,
	})
	if err != nil {
		fmt.Println("😡", err.Error())
	}
	_, _ = gl.LinkIssues(reportingIssue, interviewsIssue)

}
