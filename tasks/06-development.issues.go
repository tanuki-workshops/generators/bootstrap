package tasks

import (
	"fmt"
	"glhelper/client"
	"glhelper/models"
	"time"
)

func CreateDevelopmentIssues(gl client.GitLabClient, developmentEpic models.Epic, subDevelopmentEpic models.SubDevelopmentActivities, project models.Project, group models.Group, phases models.Phases, startDate time.Time) {

	graphicsInterfaceIssue, err := gl.PublishIssue(models.Issue{
		Title:       "Graphics and interface",
		Description: "Graphics and interface",
		CreatedAt:   startDate.Add(8 * 24 * time.Hour).Local().String(),
		Project:     project,
		Labels:      []string{"task", "development", "priority::4"},
		EpicId:      developmentEpic.Id,
		MilestoneId: phases.Construction.Id,
		Weight:      40,
	})
	if err != nil {
		fmt.Println("😡", err.Error())
	}

	contentCreationIssue, err := gl.PublishIssue(models.Issue{
		Title:       "Content creation",
		Description: "Content creation",
		CreatedAt:   startDate.Add(8 * 24 * time.Hour).Local().String(),
		Project:     project,
		Labels:      []string{"task", "development", "priority::4"},
		EpicId:      developmentEpic.Id,
		MilestoneId: phases.Construction.Id,
		Weight:      20,
	})
	if err != nil {
		fmt.Println("😡", err.Error())
	}
	_, _ = gl.LinkIssues(contentCreationIssue, graphicsInterfaceIssue)

	databaseIssue, err := gl.PublishIssue(models.Issue{
		Title:       "Database implementation",
		Description: "Database implementation",
		CreatedAt:   startDate.Add(10 * 24 * time.Hour).Local().String(),
		Project:     project,
		Labels:      []string{"task", "development", "priority::1"},
		EpicId:      developmentEpic.Id,
		MilestoneId: phases.Construction.Id,
		Weight:      30,
	})
	if err != nil {
		fmt.Println("😡", err.Error())
	}
	_, _ = gl.LinkIssues(databaseIssue, contentCreationIssue)

	catalogIssue, err := gl.PublishIssue(models.Issue{
		Title:       "Catalog engine",
		Description: "Catalog engine",
		CreatedAt:   startDate.Add(8 * 24 * time.Hour).Local().String(),
		Project:     project,
		Labels:      []string{"task", "development", "priority::1"},
		EpicId:      developmentEpic.Id,
		MilestoneId: phases.Construction.Id,
		Weight:      30,
	})
	if err != nil {
		fmt.Println("😡", err.Error())
	}
	_, _ = gl.LinkIssues(catalogIssue, databaseIssue)

	transactionIssue, err := gl.PublishIssue(models.Issue{
		Title:       "Transaction proceeding",
		Description: "Transaction proceeding",
		CreatedAt:   startDate.Add(10 * 24 * time.Hour).Local().String(),
		Project:     project,
		Labels:      []string{"task", "development", "priority::2"},
		EpicId:      developmentEpic.Id,
		MilestoneId: phases.Construction.Id,
		Weight:      40,
	})
	if err != nil {
		fmt.Println("😡", err.Error())
	}
	_, _ = gl.LinkIssues(transactionIssue, databaseIssue)

	securityIssue, err := gl.PublishIssue(models.Issue{
		Title:       "Security subsystems",
		Description: "Security subsystems",
		CreatedAt:   startDate.Add(7 * 24 * time.Hour).Local().String(),
		Project:     project,
		Labels:      []string{"task", "development", "priority::1"},
		EpicId:      developmentEpic.Id,
		MilestoneId: phases.Construction.Id,
		Weight:      40,
	})
	if err != nil {
		fmt.Println("😡", err.Error())
	}
	_, _ = gl.LinkIssues(securityIssue, transactionIssue)
	_, _ = gl.LinkIssues(securityIssue, databaseIssue)
	_, _ = gl.LinkIssues(securityIssue, catalogIssue)
	_, _ = gl.LinkIssues(securityIssue, contentCreationIssue)

	// ------------------------------------
	// Create sub development activities
	// ------------------------------------

	/* Tools */
	crudGenerators, err := gl.PublishIssue(models.Issue{
		Title:       "CRUD generators",
		Description: "CRUD generators",
		CreatedAt:   startDate.Add(3 * 24 * time.Hour).Local().String(),
		Project:     project,
		Labels:      []string{"task", "development", "priority::1"},
		EpicId:      subDevelopmentEpic.Tools.Id,
		MilestoneId: phases.Elaboration.Id,
		Weight:      10,
	})
	fmt.Println(crudGenerators)
	if err != nil {
		fmt.Println("😡", err.Error())
	}

	/* Helpers */
	loadStressScripts, err := gl.PublishIssue(models.Issue{
		Title:       "Load stress scripts",
		Description: "Load stress scripts",
		CreatedAt:   startDate.Add(3 * 24 * time.Hour).Local().String(),
		Project:     project,
		Labels:      []string{"task", "development", "priority::1"},
		EpicId:      subDevelopmentEpic.Helpers.Id,
		MilestoneId: phases.Elaboration.Id,
		Weight:      5,
	})
	fmt.Println(loadStressScripts)
	if err != nil {
		fmt.Println("😡", err.Error())
	}

	/* BusinessRules */
	implementBusinessRules, err := gl.PublishIssue(models.Issue{
		Title:       "Business rules implementation",
		Description: "Business rules implementation",
		CreatedAt:   startDate.Add(10 * 24 * time.Hour).Local().String(),
		Project:     project,
		Labels:      []string{"task", "development", "priority::1"},
		EpicId:      subDevelopmentEpic.BusinessRules.Id,
		MilestoneId: phases.Construction.Id,
		Weight:      40,
	})
	fmt.Println(implementBusinessRules)
	if err != nil {
		fmt.Println("😡", err.Error())
	}

	fixBusinessRules, err := gl.PublishIssue(models.Issue{
		Title:       "Fix business rules implementation",
		Description: "Fix business rules implementation",
		CreatedAt:   startDate.Add(20 * 24 * time.Hour).Local().String(),
		Project:     project,
		Labels:      []string{"task", "development", "priority::1"},
		EpicId:      subDevelopmentEpic.BusinessRules.Id,
		MilestoneId: phases.Transition.Id,
		Weight:      20,
	})
	fmt.Println(fixBusinessRules)
	if err != nil {
		fmt.Println("😡", err.Error())
	}

}
