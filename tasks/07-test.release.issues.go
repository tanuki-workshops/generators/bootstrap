package tasks

import (
	"fmt"
	"glhelper/client"
	"glhelper/models"
	"time"
)

func CreateTestReleaseIssues(gl client.GitLabClient, testingProductionEpic models.Epic, project models.Project, group models.Group, phases models.Phases, startDate time.Time) {

	testConfigurationIssue, err := gl.PublishIssue(models.Issue{
		Title:       "Test configuration",
		Description: "Test configuration",
		CreatedAt:   startDate.Add(3 * 24 * time.Hour).Local().String(),
		Project:     project,
		Labels:      []string{"task", "test & release", "priority::1"},
		EpicId:      testingProductionEpic.Id,
		MilestoneId: phases.Elaboration.Id,
		Weight:      15,
	})
	if err != nil {
		fmt.Println("😡", err.Error())
	}

	reviewingDesignIssue, err := gl.PublishIssue(models.Issue{
		Title:       "Reviewing design",
		Description: "Reviewing design",
		CreatedAt:   startDate.Add(4 * 24 * time.Hour).Local().String(),
		Project:     project,
		Labels:      []string{"task", "test & release", "priority::2"},
		EpicId:      testingProductionEpic.Id,
		MilestoneId: phases.Elaboration.Id,
		Weight:      15,
	})
	if err != nil {
		fmt.Println("😡", err.Error())
	}
	_, _ = gl.LinkIssues(reviewingDesignIssue, testConfigurationIssue)

	releasingIssue, err := gl.PublishIssue(models.Issue{
		Title:       "Releasing the site",
		Description: "Releasing the site",
		CreatedAt:   startDate.Add(4 * 24 * time.Hour).Local().String(),
		Project:     project,
		Labels:      []string{"task", "test & release", "priority::3"},
		EpicId:      testingProductionEpic.Id,
		MilestoneId: phases.Transition.Id,
		Weight:      10,
	})
	if err != nil {
		fmt.Println("😡", err.Error())
	}
	_, _ = gl.LinkIssues(releasingIssue, testConfigurationIssue)

	closeoutMeetingIssue, err := gl.PublishIssue(models.Issue{
		Title:       "Closeout meeting",
		Description: "Closeout meeting",
		CreatedAt:   startDate.Add(4 * 24 * time.Hour).Local().String(),
		Project:     project,
		Labels:      []string{"task", "test & release", "priority::4"},
		EpicId:      testingProductionEpic.Id,
		MilestoneId: phases.Transition.Id,
		Weight:      5,
	})
	if err != nil {
		fmt.Println("😡", err.Error())
	}
	_, _ = gl.LinkIssues(closeoutMeetingIssue, testConfigurationIssue)

	closeoutDocumentsIssue, err := gl.PublishIssue(models.Issue{
		Title:       "Closeout documents",
		Description: "Closeout documents",
		CreatedAt:   startDate.Add(4 * 24 * time.Hour).Local().String(),
		Project:     project,
		Labels:      []string{"task", "test & release", "priority::4"},
		EpicId:      testingProductionEpic.Id,
		MilestoneId: phases.Transition.Id,
		Weight:      10,
	})
	if err != nil {
		fmt.Println("😡", err.Error())
	}
	_, _ = gl.LinkIssues(closeoutDocumentsIssue, testConfigurationIssue)

}
