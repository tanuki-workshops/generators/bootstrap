package client

import (
	"encoding/json"
	"fmt"
	"glhelper/models"
)

// POST /projects
func (glc GitLabClient) PublishProject(project models.Project) (models.Project, error) {

	resp, err := restyClient.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("PRIVATE-TOKEN", glc.Token).
		SetBody(project.ToJsonString()).
		Post(glc.Url + "/projects")

	if err != nil {
		fmt.Println("😡(PublishProject)", err.Error())
		return models.Project{}, err
	}

	respProject := models.Project{}

	errJson := json.Unmarshal(resp.Body(), &respProject)
	if errJson != nil {
		fmt.Println("😡(PublishEpic-result)", errJson.Error())
		return models.Project{}, errJson
	}


	//respProject.Id = project.Id
	//respProject.Name = project.Name
	//respProject.NameSpaceId = project.NameSpaceId

	fmt.Println("🏗️", respProject)

	return respProject, nil

}
