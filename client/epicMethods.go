package client

import (
	"encoding/json"
	"fmt"
	"glhelper/models"
	"strconv"
)

// POST /groups/:id/epics
func (glc GitLabClient) PublishEpic(epic models.Epic) (models.Epic, error) {

	resp, err := restyClient.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("PRIVATE-TOKEN", glc.Token).
		SetBody(epic.ToJsonString()).
		Post(glc.Url + "/groups/" + strconv.Itoa(epic.Group.Id) + "/epics")

	if err != nil {
		fmt.Println("😡(PublishEpic)", err.Error())
		return models.Epic{}, err
	}

	respEpic := models.Epic{}

	errJson := json.Unmarshal(resp.Body(), &respEpic)
	if errJson != nil {
		fmt.Println("😡(PublishEpic-result)", errJson.Error())
		return models.Epic{}, errJson
	}

	respEpic.Title = epic.Title
	respEpic.Description = epic.Description
	respEpic.Color = epic.Color
	respEpic.CreatedAt = epic.CreatedAt
	respEpic.Group = epic.Group
	respEpic.Labels = epic.Labels
	//respEpic.ParentId = epic.ParentId
	respEpic.StartDateIsFixed = epic.StartDateIsFixed
	respEpic.StartDateFixed = epic.StartDateFixed

	return respEpic, nil

}
