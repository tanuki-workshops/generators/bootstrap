package client

import (
	"encoding/json"
	"fmt"
	"glhelper/models"
)

// POST /groups
func (glc GitLabClient) PublishGroup(group models.Group) (models.Group, error) {

	resp, err := restyClient.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("PRIVATE-TOKEN", glc.Token).
		SetBody(group.ToJsonString()).
		Post(glc.Url + "/groups")

	if err != nil {
		fmt.Println("😡(PublishGroup)", err.Error())
		return models.Group{}, err
	}
	
	respGroup := models.Group{}

	//fmt.Println(string(resp.Body()))

	errJson := json.Unmarshal(resp.Body(), &respGroup)
	if errJson != nil {
		fmt.Println("😡(PublishEpic-result)", errJson.Error())
		return models.Group{}, errJson
	}


	//respGroup.Id = group.Id
	//respGroup.Name = group.Name
	//respGroup.Path = group.Path
	//respGroup.ParentId = group.ParentId

	fmt.Println("🏘️", respGroup)

	return respGroup, nil

}
