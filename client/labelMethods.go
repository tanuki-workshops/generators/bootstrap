package client

import (
	"fmt"
	"glhelper/models"
	"strconv"
)

// Project label
// POST /projects/:id/labels
func (glc GitLabClient) PublishProjectLabel(label models.Label, project models.Project) (string, error) {

	resp, err := restyClient.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("PRIVATE-TOKEN", glc.Token).
		SetBody(label.ToJsonString()).
		Post(glc.Url + "/projects/" + strconv.Itoa(project.Id) + "/labels")

	if err != nil {
		fmt.Println("😡(PublishProjectLabel)", err.Error())
		return "{}", err
	}

	return string(resp.Body()), nil
}

// Group label
// POST /groups/:id/labels
func (glc GitLabClient) PublishGroupLabel(label models.Label, group models.Group) (string, error) {

	/*
		resp, err := restyClient.R().
			SetHeader("Content-Type", "application/json").
			SetHeader("PRIVATE-TOKEN", glc.Token).
			SetBody(`{"name":"` + label.Name + `", "color":"` + label.Color + `", "description":"` + label.Description + `"}`).
			Post(glc.Url + "/groups/" + strconv.Itoa(group.Id) + "/labels")
	*/
	resp, err := restyClient.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("PRIVATE-TOKEN", glc.Token).
		SetBody(label.ToJsonString()).
		Post(glc.Url + "/groups/" + strconv.Itoa(group.Id) + "/labels")

	/*
		resp, err := glc.Post(
			"/groups/"+strconv.Itoa(group.Id)+"/labels",
			label.ToJsonString(),
		)
	*/

	if err != nil {
		fmt.Println("😡(PublishGroupLabel)", err.Error())
		return "{}", err
	} else {
		fmt.Println(string(resp.Body()))
	}

	return string(resp.Body()), nil

}
