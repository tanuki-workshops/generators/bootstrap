package client

import (
	"encoding/json"
	"fmt"
	"glhelper/models"
	"strconv"
)

func (glc GitLabClient) PublishIssue(issue models.Issue) (models.Issue, error) {

	resp, err := restyClient.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("PRIVATE-TOKEN", glc.Token).
		SetBody(issue.ToJsonString()).
		Post(glc.Url + "/projects/" + strconv.Itoa(issue.Project.Id) + "/issues")

	if err != nil {
		fmt.Println("😡(PublishIssue)", err.Error())
		return models.Issue{}, err
	}

	

	respIssue := models.Issue{}

	//fmt.Println(string(resp.Body()))

	errJson := json.Unmarshal(resp.Body(), &respIssue)
	if errJson != nil {
		fmt.Println("😡(PublishIssue-result)", errJson.Error())
		return models.Issue{}, errJson
	}

	respIssue.Title = issue.Title
	respIssue.Description = issue.Description
	respIssue.CreatedAt = issue.CreatedAt
	respIssue.DueDate = issue.DueDate
	respIssue.Project = issue.Project
	respIssue.Labels = issue.Labels
	respIssue.EpicId = issue.EpicId
	respIssue.MilestoneId = issue.MilestoneId
	respIssue.Weight = issue.Weight

	fmt.Println("📝 Issue Created", respIssue)
	fmt.Println("    Id, IId", respIssue.Id, respIssue.IId)
	fmt.Println("    EpicId", respIssue.EpicId)
	fmt.Println("    MilestoneId", respIssue.MilestoneId)


	return respIssue, nil

}

//POST /projects/:id/issues/:issue_iid/links

func (glc GitLabClient) LinkIssues(issue1, issue2 models.Issue) (string, error) {

	resp, err := restyClient.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("PRIVATE-TOKEN", glc.Token).
		SetBody(`{"target_project_id":"` + strconv.Itoa(issue2.Project.Id) + `", "target_issue_iid":"` + strconv.Itoa(issue2.IId) + `", "link_type":"relates_to"}`).
		Post(glc.Url + "/projects/" + strconv.Itoa(issue1.Project.Id) + "/issues/" + strconv.Itoa(issue1.IId) + "/links")

	if err != nil {
		fmt.Println("😡(LinkIssues)", err.Error())
		return "{}", err
	}

	return string(resp.Body()), nil

}

// PUT /projects/:id/issues/:issue_iid
func (glc GitLabClient) UpdateIssue(issue models.Issue) (string, error) {

	url := glc.Url + "/projects/" + strconv.Itoa(issue.Project.Id) + "/issues/" + strconv.Itoa(issue.IId)

	resp, err := restyClient.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("PRIVATE-TOKEN", glc.Token).
		SetBody(issue.ToJsonString()).
		Put(url)

	if err != nil {
		fmt.Println("😡(UpdateIssue)", err.Error())
		return "{}", err
	}

	return string(resp.Body()), nil
}
