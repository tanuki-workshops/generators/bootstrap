package client

import "github.com/go-resty/resty/v2"

type GitLabClient struct {
	Token string
	Url   string
}

var (
	restyClient = resty.New()
)

/*
func (glc GitLabClient) Post(url string, body interface{}) (*resty.Response, error) {
	resp, err := restyClient.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("PRIVATE-TOKEN", glc.Token).
		SetBody(body).
		Post(glc.Url)

	return resp, err
}

func (glc GitLabClient) Put(url string, body interface{}) (*resty.Response, error) {
	resp, err := restyClient.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("PRIVATE-TOKEN", glc.Token).
		SetBody(body).
		Put(glc.Url)

	return resp, err
}
*/