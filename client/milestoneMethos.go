package client

import (
	"encoding/json"
	"fmt"
	"glhelper/models"
	"strconv"
)

/*
Project Milestone
POST /projects/:id/milestones
*/
func (glc GitLabClient) PublishProjectMilestone(milestone models.Milestone) (models.Milestone, error) {

	resp, err := restyClient.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("PRIVATE-TOKEN", glc.Token).
		SetBody(milestone.ToJsonString()).
		Post(glc.Url + "/projects/" + strconv.Itoa(milestone.Project.Id) + "/milestones")

	if err != nil {
		fmt.Println("😡(PublishProjectMilestone)", err.Error())
		return models.Milestone{}, err
	}

	respMilestone := models.Milestone{}
	errJson := json.Unmarshal(resp.Body(), &respMilestone)
	if errJson != nil {
		fmt.Println("😡(PublishProjectMilestone-result)", errJson.Error())
		return models.Milestone{}, errJson
	}
	respMilestone.Description = milestone.Description
	respMilestone.DueDate = milestone.DueDate
	respMilestone.Group = milestone.Group
	//respMilestone.Id = milestone.Id
	respMilestone.Project = milestone.Project
	respMilestone.StartDate = milestone.StartDate
	respMilestone.Title = milestone.Title

	return respMilestone, nil
}

/*
Project Milestone
POST /groups/:id/milestones
*/
func (glc GitLabClient) PublishGroupMilestone(milestone models.Milestone) (models.Milestone, error) {

	resp, err := restyClient.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("PRIVATE-TOKEN", glc.Token).
		SetBody(milestone.ToJsonString()).
		Post(glc.Url + "/groups/" + strconv.Itoa(milestone.Group.Id) + "/milestones")

	if err != nil {
		fmt.Println("😡(PublishGroupMilestone)", err.Error())
		return models.Milestone{}, err
	}

	respMilestone := models.Milestone{}
	errJson := json.Unmarshal(resp.Body(), &respMilestone)
	if errJson != nil {
		fmt.Println("😡(PublishGroupMilestone-result)", errJson.Error())
		return models.Milestone{}, errJson
	}
	respMilestone.Description = milestone.Description
	respMilestone.DueDate = milestone.DueDate
	respMilestone.Group = milestone.Group
	respMilestone.Id = milestone.Id
	respMilestone.Project = milestone.Project
	respMilestone.StartDate = milestone.StartDate
	respMilestone.Title = milestone.Title

	return respMilestone, nil
}
