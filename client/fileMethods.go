package client

import (
	"encoding/json"
	"fmt"
	"glhelper/models"
	"strconv"
)

// POST /projects/:id/repository/files/:file_path

func (glc GitLabClient) PublishFile(file models.File) (models.File, error) {

	fmt.Println(file)

	resp, err := restyClient.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("PRIVATE-TOKEN", glc.Token).
		SetBody(file.ToJsonString()).
		Post(glc.Url + "/projects/" + strconv.Itoa(file.ProjectId) + "/repository/files/" + file.FilePath)

	if err != nil {
		fmt.Println("😡(PublishFile)", err.Error())
		return models.File{}, err
	}

	//fmt.Println(resp.Body())

	respFile := models.File{}

	errJson := json.Unmarshal(resp.Body(), &respFile)
	if errJson != nil {
		fmt.Println("😡(PublishFile-result)", errJson.Error())
		return models.File{}, errJson
	}

	return respFile, nil

}
