package main

import (
	"glhelper/client"
	"glhelper/models"
	"glhelper/tasks"
	"log"
	"os"
	"strings"
	"time"
)

/*
var (
	projectId     = 51502311 //51418507
	parentGroupId = 76822587 //76688840
)
*/

var (
	groupName   = "Eden-31"
	mainGroupId = 5085244 // main organisation
	projectName = "demo"

	startDate = time.Date(2023, time.October, 30, 0, 0, 0, 0, time.Local)

	gl = client.GitLabClient{Token: os.Getenv("GITLAB_TOKEN_ADMIN"), Url: "https://gitlab.com/api/v4"}
)

func main() {

	//project := models.Project{Id: projectId}

	group, err := gl.PublishGroup(models.Group{
		Name:       groupName,
		ParentId:   mainGroupId,
		Path:       strings.ToLower(groupName),
		Visibility: "public",
	})

	if err != nil {
		log.Panic("😡 when creating the group")
	}

	project, err := gl.PublishProject(models.Project{
		Name:                 projectName,
		NameSpaceId:          group.Id,
		InitializeWithReadMe: false,
		Visibility:           "public",
	})

	if err != nil {
		log.Panic("😡 when creating the group")
	}

	tasks.CreateLabels(gl, group, project)

	// Roadmap

	mainActivitiesEpic, subDevelopmentActivitiesEpic, _ := tasks.CreateEpics(gl, group)

	// Milestones
	
	milestones, _ := tasks.CreateProjectMilestones(gl, project, startDate)

	tasks.CreateProjectManagementIssues(gl, mainActivitiesEpic.ProjectManagement, project, group, milestones, startDate)

	tasks.CreateAnalysisIssues(gl, mainActivitiesEpic.Analysis, project, group, milestones, startDate)

	tasks.CreateDesignIssues(gl, mainActivitiesEpic.Design, project, group, milestones, startDate)

	tasks.CreateDevelopmentIssues(gl, mainActivitiesEpic.Development, subDevelopmentActivitiesEpic, project, group, milestones, startDate)

	tasks.CreateTestReleaseIssues(gl, mainActivitiesEpic.TestRelease, project, group, milestones, startDate)

}
