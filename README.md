# bootstrap

## How to

Update `main.go`:

```golang
var (
	groupName   = "demo-group" // this group will be created
	mainGroupId = 5085244   // Id of the main organisation
                          // this is the id of tanuki-workshops
	projectName = "demo"    // Name of the demo project

  // Star date
  startDate = time.Date(2023, time.October, 15, 0, 0, 0, 0, time.Local)

  // you need an environment variable GITLAB_TOKEN_ADMIN with a PAT
  gl = client.GitLabClient{Token: os.Getenv("GITLAB_TOKEN_ADMIN"), Url: "https://gitlab.com/api/v4"}

)
```

Then run `go run main.go`
